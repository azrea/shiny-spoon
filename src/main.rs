use std::borrow::Cow;
use std::fmt::Display;
use std::future;
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{bail, Context, Result};
use clap::{arg, ArgAction, ArgMatches, Args, Parser, Subcommand, ValueEnum};
use directories::ProjectDirs;
use futures::{Stream, StreamExt, TryStreamExt};
use glob::glob;
use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;
use sqlx::sqlite::{
    SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteRow, SqliteSynchronous,
};
use sqlx::{FromRow, Pool, Row, Sqlite};
use starsector::Arena;
use starsector_async_parse::starsector_async_parse;
use struson::writer::{JsonStreamWriter, JsonWriter};
use time::format_description::well_known::Iso8601;
use time::OffsetDateTime;
use tokio::fs::{self, File};
use tokio::io::BufReader;
use tokio::sync::Semaphore;
use uuid::Uuid;

struct OrgFile {
    id: Uuid,
    path: PathBuf,
    last_modified: Option<OffsetDateTime>,
}

impl OrgFile {
    async fn try_get_org_file(pool: &Pool<Sqlite>, path: &Path) -> Result<Option<Self>> {
        sqlx::query_as::<_, Self>("select * from files where path = ?")
            .bind(path.to_str().unwrap())
            .fetch_optional(pool)
            .await
            .context("While finding a file record")
    }

    async fn get_org_file(pool: &Pool<Sqlite>, path: &Path) -> Result<Self> {
        Ok(Self::try_get_org_file(pool, path)
            .await?
            .unwrap_or_else(|| Self {
                id: Uuid::new_v4(),
                path: path.to_path_buf(),
                last_modified: None,
            }))
    }

    async fn upsert(&self, pool: &Pool<Sqlite>) -> Result<()> {
        sqlx::query("insert into files (id, path, last_modified) values (?, ?, ?) on conflict (path) do update set last_modified = excluded.last_modified ")
            .bind(self.id)
            .bind(self.path.to_str())
            .bind(self.last_modified.map(|lm| lm.format(&Iso8601::DEFAULT).unwrap()))
            .execute(pool)
            .await
            .context("inserting file record")?;
        Ok(())
    }

    async fn purge_related_records(&self, pool: &Pool<Sqlite>) -> Result<()> {
        // Deletes sections via database trigger
        sqlx::query("delete from headlines where file = ?")
            .bind(self.id)
            .execute(pool)
            .await
            .context("deleting related records")?;
        Ok(())
    }
}

impl FromRow<'_, SqliteRow> for OrgFile {
    fn from_row(row: &SqliteRow) -> sqlx::Result<Self> {
        Ok(Self {
            id: row.try_get("id")?,
            path: row.try_get::<&str, &str>("path")?.into(),
            last_modified: row
                .try_get::<Option<String>, &str>("last_modified")?
                .map(|lm| OffsetDateTime::parse(&lm, &Iso8601::DEFAULT).unwrap()),
        })
    }
}

async fn process_file(pool: Pool<Sqlite>, path: PathBuf) -> Result<()> {
    let mut arena = Arena::default();

    let file = File::open(&path).await.context("Couldn't find org file")?;

    let last_modified: OffsetDateTime = file
        .metadata()
        .await
        .context("Getting file metadata")?
        .modified()
        .context("Getting file modified at")?
        .into();

    let mut org_file = OrgFile::get_org_file(&pool, &path)
        .await
        .context("While finding file record")?;

    //dbg!((Some(last_modified), org_file.last_modified));
    if Some(last_modified) == org_file.last_modified {
        return Ok(());
    }

    org_file.last_modified = Some(last_modified);

    org_file.upsert(&pool).await.context("Saving file record")?;

    org_file
        .purge_related_records(&pool)
        .await
        .context("Cleaning up before reparsing file")?;

    let mut buf_reader = BufReader::new(file);

    let doc = starsector_async_parse(&mut arena, &mut buf_reader)
        .await
        .context("Parsing org file")?;

    let doc_rope = doc.root.to_rope(&arena);

    for s in doc.root.descendants(&arena) {
        let h_uuid = Uuid::new_v4();
        let offset = doc
            .text_offset_of_child(&arena, s)
            .context("Finding the char offset")?;
        let line = if let Ok(l) = doc_rope.try_char_to_line(offset) {
            l
        } else {
            continue;
        };

        sqlx::query("insert into headlines (id, file, line, offset) values (?, ?, ?, ?)")
            .bind(h_uuid)
            .bind(org_file.id)
            .bind(line as u32)
            .bind(offset as u32)
            //.bind(&s.title(&arena, None).unwrap())
            .execute(&pool)
            .await
            .context("Inserting headlines")?;

        if let Ok(body) = s.body(&arena, None) {
            sqlx::query("insert into sections (uuid, title, content) values (?, ?, ?)")
                .bind(h_uuid)
                .bind(s.headline(&arena, None).map(|h| h.title().to_string()))
                .bind(body)
                .execute(&pool)
                .await
                .context("Inserting section text")?;
        };
    }

    Ok(())
}

struct AppConfig {
    config: PathBuf,
    path: Option<PathBuf>,
}

impl AppConfig {
    fn path_path(config: &Path) -> PathBuf {
        config.join("path")
    }

    async fn read(config: &Path) -> Self {
        Self {
            config: config.to_path_buf(),
            path: fs::read_to_string(Self::path_path(config))
                .await
                .map(|p| p.trim().into())
                .ok(),
        }
    }

    async fn write(&self) -> Result<()> {
        if let Some(p) = &self.path {
            let config_path = Self::path_path(&self.config);

            fs::create_dir_all(config_path.parent().unwrap())
                .await
                .context("Couldn't create database file")?;

            fs::write(config_path, p.to_str().context("Converting path")?)
                .await
                .context("While writing out configuration")?
        }

        Ok(())
    }
}

async fn reset_database(database_path: &Path) {
    println!("Resetting database");
    let _ = fs::remove_file(&database_path).await;
}

#[derive(Parser, Debug)]
#[clap(version, author = "Azrea Amis <azrea@azrea.me>")]
struct AppArgs {
    #[clap(subcommand)]
    command: Commands,

    #[clap(short, long, action = ArgAction::SetTrue)]
    no_update: bool,

    #[clap(short, long, value_parser = clap::value_parser!(u32), default_value_t = 50)]
    limit: u32,
}

#[derive(Subcommand, Debug, Clone)]
enum Commands {
    ResetCache,
    SetPath {
        #[clap(required = true)]
        path: PathBuf,
    },
    GetPath,
    Search {
        #[clap(short, long, action = ArgAction::SetTrue)]
        no_update: bool,

        #[clap(short, long, value_parser = clap::value_parser!(u32), default_value_t = 50)]
        limit: u32,

        /// Override user configuration
        #[clap(short = 'd', long, value_parser = clap::value_parser!(PathBuf), env = "SHINY_SPOON_DIR")]
        search_dir: Option<PathBuf>,

        #[clap(short, long, value_parser, default_value_t = OutputFormat::Table)]
        output: OutputFormat,

        #[clap(required = true)]
        query: Vec<String>,
    },
}

#[derive(Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Clone, ValueEnum)]
enum OutputFormat {
    Table,
    Json,
    JsonObj,
}

impl Display for OutputFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            OutputFormat::Table => "table",
            OutputFormat::Json => "json",
            OutputFormat::JsonObj => "json-obj",
        })
    }
}

impl FromStr for OutputFormat {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        use OutputFormat::*;

        Ok(match s {
            "table" => Table,
            "json" => Json,
            "json-obj" => JsonObj,
            _ => bail!("Failed to parse output format"),
        })
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let dirs = ProjectDirs::from("me", "azrea", "shiny-spoon").unwrap();

    // dbg!(&dirs);

    // This have dynamic default values based on ProjectDirs, so we mix derive
    // and builder arguments. This is a little buggy.
    let cli = clap::Command::new("shiny-spoon")
        .arg(
            arg!(--"config-dir" <DIR> "Config directory")
                .takes_value(true)
                .value_name("DIR")
                .value_parser(clap::value_parser!(PathBuf))
                .default_value(dirs.config_dir().to_str().unwrap())
                .required(false)
                .action(clap::ArgAction::Set),
        )
        .arg(
            arg!(--"cache-dir" <DIR> "Cache directory")
                .takes_value(true)
                .value_name("DIR")
                .value_parser(clap::value_parser!(PathBuf))
                .default_value(dirs.cache_dir().to_str().unwrap())
                .required(false)
                .action(clap::ArgAction::Set),
        );

    let cli = AppArgs::augment_args(cli);

    let args = cli.get_matches();

    // dbg!(&args);

    let mut config = AppConfig::read(args.get_one::<PathBuf>("config-dir").unwrap()).await;

    let database_file = args
        .get_one::<PathBuf>("cache-dir")
        .unwrap()
        .join("shiny_spoon.sqlite3");

    fs::create_dir_all(database_file.parent().unwrap())
        .await
        .context("Couldn't create database file")?;

    let pool_timeout = Duration::from_secs(30);
    // with pool_max_connections = 1, the pool timeout. maybe related to https://github.com/launchbadge/sqlx/issues/1210
    let pool_max_connections = 5;

    // if args.get_flag("reset") {
    //     reset_database(&database_file);
    // }

    let connection_options = SqliteConnectOptions::new()
        .filename(&database_file)
        .create_if_missing(true)
        .journal_mode(SqliteJournalMode::Wal)
        .synchronous(SqliteSynchronous::Normal)
        .busy_timeout(pool_timeout);

    let sqlite_pool = SqlitePoolOptions::new()
        .max_connections(pool_max_connections)
        .connect_timeout(pool_timeout)
        .connect_with(connection_options)
        .await?;

    sqlx::migrate!("./db").run(&sqlite_pool).await?;

    match args.subcommand_name().unwrap() // required
    {
        "reset-cache" => {
            reset_database(&database_file).await;
            Ok(())
        }
        "set-path" => {
            config.path = Some(args.subcommand_matches("set-path").expect("Getting subcommand matches").get_one::<String>("path").expect("Getting path").into());
            config.write().await.context("While setting path")?;
            Ok(())
        }
        "get-path" => {
            match &config.path {
                Some(p) => println!("Configured path: {:}", p.to_string_lossy()),
                None => println!("No configured path"),
            }

            Ok(())
        }
        "search" => default_command(&sqlite_pool, args.subcommand_matches("search").expect("Getting subcommand matches"), &config).await,
        _ => panic!("Subcommand not recognized!"),
    }?;

    sqlite_pool.close().await;

    Ok(())
}

fn strip_org_links(s: &str) -> Cow<'_, str> {
    lazy_static! {
        // Org links look like: [[link][title]], and we want to replace the whole link with just the title
        static ref RE: Regex = Regex::new(r#"\[\[[^\]]+\]\[([^\]]+)\]\]"#).unwrap();
    }

    let result = RE.replace_all(s, "$1");

    result
}

#[cfg(test)]
#[test]
fn test_strip_org_links() {
    assert_eq!(strip_org_links("[[throwaway][real]]"), "real");
    assert_eq!(
        strip_org_links("[[throwaway][real1]] [[throwaway][real2]]"),
        "real1 real2"
    );
    assert_eq!(
        strip_org_links("[[throwaway][real]] also real"),
        "real also real"
    );
}

struct SearchMetadata {
    files: i64,
    headlines: i64,
    sections: i64,
    updated: bool,
}

async fn default_command(
    sqlite_pool: &Pool<Sqlite>,
    args: &ArgMatches,
    config: &AppConfig,
) -> Result<()> {
    let do_update = !args.get_flag("no-update");

    if !do_update {
        let mut handles = vec![];

        let sem = Arc::new(Semaphore::new(10));

        // TODO we need to detect changed files, and this can be done more efficiently
        // TODO handle deleted files
        for path in glob(
            &args
                .get_one::<PathBuf>("search-dir")
                .or(config.path.as_ref())
                .map(|p| p.join("**/*.org"))
                .map(|p| p.to_string_lossy().to_string())
                .ok_or_else(|| anyhow::format_err!("No search directory specified"))?,
        )?
        .flatten()
        {
            let pool = sqlite_pool.clone();
            let permit = Arc::clone(&sem).acquire_owned().await;

            handles.push(tokio::spawn(async move {
                let _permit = permit;
                process_file(pool, path).await
            }));
        }

        for h in handles {
            h.await
                .context("Joining workers")?
                .context("Processing file")?;
        }
    }

    let (files, headlines, sections) = sqlx::query_as::<_, (i64, i64, i64)>(
        "select
            (select count(*) from files),
            (select count(*) from headlines),
            (select count(*) from sections);",
    )
    .fetch_one(sqlite_pool)
    .await
    .unwrap();

    let meta = SearchMetadata {
        files,
        headlines,
        sections,
        updated: do_update,
    };

    let query = &args.get_many::<String>("query").unwrap().join(" "); // required

    let rows = fts_search(
        sqlite_pool,
        query,
        *args.get_one::<u32>("limit").unwrap(), // has default
    );

    match args.get_one::<OutputFormat>("output").unwrap() // has default
        {
            OutputFormat::Table => table_output(&meta, rows).await.context("Outputting table format")?,
            OutputFormat::Json => json_output(&meta, rows).await.context("Outputting JSON format")?,
            OutputFormat::JsonObj => json_obj_output(&meta, rows).await.context("Outputting JSON object format")?,
        };

    Ok(())
}

async fn table_output(
    meta: &SearchMetadata,
    rows: impl Stream<Item = Result<SearchResult, sqlx::Error>>,
) -> Result<()> {
    struct FormattedSearchResult {
        location: String,
        bm25: f64,
        short_title: String,
        snippet: String,
    }

    if !meta.updated {
        println!("Skipping update");
    }

    println!(
        "files {:?} headlines {:?} sections {:?}",
        meta.files, meta.headlines, meta.sections
    );
    let formatted_rows: Vec<_> = rows
        .map_ok(|sr| {
            let max = 50;

            // This sometime stops after a space, then adds the ...
            let mut short_title = String::with_capacity(max);

            short_title.extend(strip_org_links(&sr.title).chars().take(max - 3));

            if short_title.len() == max - 3 {
                short_title.push_str("...");
            }

            FormattedSearchResult {
                location: format!("{:}:{:}", sr.path, sr.line),
                bm25: sr.bm25,
                short_title,
                snippet: sr.snippet,
            }
        })
        .try_collect()
        .await
        .context("Searching the database")?;

    let location_length = formatted_rows
        .iter()
        .map(|r| r.location.len())
        .max()
        .unwrap_or_default();
    let short_title_length = formatted_rows
        .iter()
        .map(|r| r.short_title.len())
        .max()
        .unwrap_or_default();

    for row in formatted_rows {
        println!(
            "{:location_width$}\t{:.5}\t{:short_title_width$}\t{:?}",
            row.location,
            row.bm25,
            row.short_title,
            row.snippet,
            location_width = location_length,
            short_title_width = short_title_length,
        );
    }

    Ok(())
}

async fn json_output(
    meta: &SearchMetadata,
    mut rows: impl Stream<Item = Result<SearchResult, sqlx::Error>> + std::marker::Unpin,
) -> Result<()> {
    let mut stdout = std::io::stdout().lock();
    let mut ser = JsonStreamWriter::new(&mut stdout);

    ser.begin_object()?;

    ser.name("updated")?;
    ser.bool_value(meta.updated)?;

    ser.name("files")?;
    ser.number_value(meta.files)?;
    ser.name("headlines")?;
    ser.number_value(meta.headlines)?;
    ser.name("sections")?;
    ser.number_value(meta.sections)?;

    ser.name("results")?;
    ser.begin_array()?;

    while let Some(r) = rows.try_next().await? {
        ser.serialize_value(&r)?;
    }

    ser.end_array()?;

    // map.serialize_key("rows")?;
    // let mut row_seq = map.serialize_seq(None);

    ser.end_object()?;
    // Ensures that the JSON document is complete and flushes the buffer
    ser.finish_document()?;
    todo!()
}

async fn json_obj_output(
    _meta: &SearchMetadata,
    rows: impl Stream<Item = Result<SearchResult, sqlx::Error>>,
) -> Result<()> {
    rows.for_each(|r| {
        println!("{:}", serde_json::to_string(&r.unwrap()).unwrap());
        future::ready(())
    })
    .await;

    Ok(())
}

#[derive(sqlx::FromRow, Debug, Serialize)]
struct SearchResult {
    path: String,
    line: u32,
    bm25: f64,
    title: String,
    snippet: String,
}

fn fts_search<'a>(
    pool: &'a Pool<Sqlite>,
    query: &'a str,
    number: u32,
) -> Pin<Box<dyn futures_core::stream::Stream<Item = Result<SearchResult, sqlx::Error>> + Send + 'a>>
{
    sqlx::query_as::<_, SearchResult>(
        "
select files.path,
       headlines.line,
       bm25(sections) bm25,
       sections.title,
       snippet(sections, -1, '', '', '...', 20) snippet
from sections
join headlines on headlines.id = sections.uuid
join files on files.id = headlines.file
where sections match ? order by rank asc limit ?",
    )
    .bind(query)
    .bind(number)
    .fetch(pool)
}
