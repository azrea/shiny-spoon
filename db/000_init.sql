create table files (
       id blob primary key not null,
       path text not null,
       last_modified text
);

create unique index idx_files_on_id on files(id);
create unique index idx_files_on_path on files(path);

create table headlines (
       id blob primary key not null,
       file blob,
       line int not null,
       offset int not null,
       foreign key(file) references files(id)
);

create unique index idx_headlines_on_id on headlines(id);

create virtual table sections using fts5 (uuid unindexed, title, content, tokenize = 'porter unicode61');

create trigger delete_sections
before delete on headlines
for each row
    begin delete from sections where sections.uuid = old.id;
end;
