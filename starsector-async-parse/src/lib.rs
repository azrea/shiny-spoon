use std::str::Utf8Error;

use ropey::RopeBuilder;
use starsector::{Arena, Document};
use thiserror::Error;
use tokio::io::{self, AsyncBufRead, AsyncBufReadExt};

#[derive(Error, Debug)]
pub enum SapError {
    #[error("IO error while reading async stream")]
    IOError(#[from] io::Error),

    #[error("Async stream contained invalid UTF8 data")]
    Utf8Error(#[from] Utf8Error),
}

pub async fn starsector_async_parse(
    arena: &mut Arena,
    reader: &mut (impl AsyncBufRead + Unpin),
) -> Result<Document, SapError> {
    let mut rope = RopeBuilder::new();

    // Code adapted from ropey's from_reader
    loop {
        let buf = reader.fill_buf().await?;

        if buf.is_empty() {
            break;
        }

        let valid_count = match std::str::from_utf8(buf) {
            Ok(_) => buf.len(),
            Err(e) => e.valid_up_to(),
        };
        // Append the valid part of the buffer to the rope.
        if valid_count > 0 {
            // The unsafe block here is reinterpreting the bytes as
            // utf8.  This is safe because the bytes being
            // reinterpreted have already been validated as utf8
            // just above.
            rope.append(unsafe { std::str::from_utf8_unchecked(&buf[..valid_count]) });
        } else {
            std::str::from_utf8(buf)?;
        }

        reader.consume(valid_count)
    }

    Ok(arena.parse_rope(rope.finish()))
}
